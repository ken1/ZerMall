package com.dfhz.ken.zermall.UI.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.dfhz.ken.zermall.R;
import com.dfhz.ken.zermall.UI.activity.shopins.FabuShopinActivity;
import com.dfhz.ken.zermall.UI.activity.shopins.FabuShopinActivity2;
import com.dfhz.ken.zermall.UI.base.FragmentTabAdapter;
import com.dfhz.ken.zermall.UI.fragment.FirstFragment;
import com.dfhz.ken.zermall.UI.fragment.MineFragment;
import com.dfhz.ken.zermall.utils.L;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends FragmentActivity {

    @Bind(R.id.radio_button1)
    RadioButton radioButtonLeft;
    @Bind(R.id.img_creat)
    ImageView imgCreat;
    @Bind(R.id.radio_button3)
    RadioButton radioButtonRight;
    @Bind(R.id.group)
    RadioGroup group;
    @Bind(R.id.tab_content)
    FrameLayout tabContent;
    /**
     * 退出时间
     */
    private long mExitTime;
    /**
     * 退出间隔
     */
    private static final int INTERVAL = 2000;

    private FirstFragment mFirstFragment;
    private MineFragment mMineFragment;
    public List<Fragment> fragments = new ArrayList<Fragment>();
    private FragmentTabAdapter tabAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        initBeforeData();
        initEvents();
        initAfterData();
    }

    private void initBeforeData() {
        addTabIntent();
    }

    private void initEvents() {
        imgCreat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,Test2Avtivity.class);
                startActivity(intent);
            }
        });
    }

    private void initAfterData() {
    }

    private void addTabIntent() {
        mFirstFragment = new FirstFragment();
        mMineFragment = new MineFragment();
        fragments.add(mFirstFragment);
        fragments.add(new Fragment());
        fragments.add(mMineFragment);

        tabAdapter = new FragmentTabAdapter(this, fragments, R.id.tab_content, group);
        tabAdapter.setOnRgsExtraCheckedChangedListener(new FragmentTabAdapter.OnRgsExtraCheckedChangedListener() {
            @Override
            public void OnRgsExtraCheckedChanged(RadioGroup radioGroup, int checkedId, int index) {
                switch (index) {
                    case 0:
                        L.e("currentTabIndex", "0000");
                        break;
                    case 1:
                        L.e("currentTabIndex", "1111");
                        break;
                    case 2:
                        L.e("currentTabIndex", "2222");
                        break;
                    default:

                        break;
                }

            }
        });
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            if (event.getAction() == KeyEvent.ACTION_DOWN
                    && event.getRepeatCount() == 0) {
                    exit();
            }
            return true;
        }
        return super.dispatchKeyEvent(event);
    }

    /**
     * 判断两次返回时间间隔,小于两秒则退出程序
     */
    private void exit() {
        if (System.currentTimeMillis() - mExitTime > INTERVAL) {
            Toast.makeText(this, "再按一次返回退出应用", Toast.LENGTH_SHORT).show();
            mExitTime = System.currentTimeMillis();
        } else {
            android.os.Process.killProcess(android.os.Process.myPid());
            System.exit(0);
        }
    }


}
