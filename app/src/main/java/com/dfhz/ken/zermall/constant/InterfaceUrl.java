package com.dfhz.ken.zermall.constant;


public class InterfaceUrl {

    public static String ip = "192.168.0.119:8080/foldcat/";   //测试服务器
//    	public static String ip= "www.defenghuizhi.com/rckserver";  //正式服务器
//	public static String ip= "123.56.179.170:9090/rckserver";  //正式服务器  (测试)

    //获取商品品牌列表
    public static String getBrandList = "http://" + ip + "goods/getbrandlist.do";
    //获取商品类别列表
    public static String getGoodStypeList = "http://" + ip + "banner/getgoodstypelist.do?";
    //发布商品
    public static String addGoods = "http://" + ip + "goods/addgoods.do?";

}
