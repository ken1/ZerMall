package com.dfhz.ken.zermall.UI.fragment;

import com.dfhz.ken.zermall.R;
import com.dfhz.ken.zermall.UI.base.BaseFragment;

/**
 * Created by huangyuekai on 16/11/17.
 */
public class MineFragment extends BaseFragment {
    @Override
    protected void setLayoutId() {
        layoutId = R.layout.fragment_mine;
    }

    @Override
    protected void initBeforeData() {

    }

    @Override
    protected void initEvents() {

    }

    @Override
    protected void initAfterData() {

    }
}
