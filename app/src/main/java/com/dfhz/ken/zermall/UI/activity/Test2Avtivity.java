package com.dfhz.ken.zermall.UI.activity;

import android.app.Activity;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.dfhz.ken.zermall.R;
import com.dfhz.ken.zermall.UI.adapter.FansAdapter;
import com.dfhz.ken.zermall.UI.adapter.ShopinAdapter;
import com.dfhz.ken.zermall.utils.L;
import com.dfhz.ken.zermall.utils.network.NetWorkUtil;

import java.util.ArrayList;
import java.util.List;

import widget.listview.FadingRefreshLayout;
import widget.swipemenulistview.SwipeMenuListView;

/**
 * Created by huangyuekai on 16/11/18.
 */
public class Test2Avtivity extends Activity {
    SwipeMenuListView listview;
    FadingRefreshLayout refreshLayout;
    LinearLayout mLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test1);

        listview = (SwipeMenuListView)findViewById(R.id.listview);
        refreshLayout = (FadingRefreshLayout)findViewById(R.id.refreshLayout);
        ShopinAdapter adapter = new ShopinAdapter(this);
        List<ShopinAdapter.ResultBean> mList = new ArrayList<>();

        for(int i =0 ;i < 20;i++){
            mList.add(new ShopinAdapter.ResultBean());
        }
        adapter.appendToList(mList);
        listview.setAdapter(adapter);
        initLinearLayout();
        NetWorkUtil.getBrandList(new NetWorkUtil.SCallBack() {
            @Override
            public void onSuccess(String result) {
                L.e("getBrandList",result);
            }

            @Override
            public void onFailure(String result) {

            }
        });

    }

    void initLinearLayout() {
        //��ʼ��fadingScrollView��һЩ��Ϣ
        mLayout = (LinearLayout) findViewById(R.id.lyout_title);
        ColorDrawable bgDrawable = new ColorDrawable(getResources().getColor(
                R.color.white));
        refreshLayout.bindingActionBar(mLayout);// ���ý��䲼��
        refreshLayout.setActionBarBgDrawable(bgDrawable);// ���ý�����ɫ
    }
}
