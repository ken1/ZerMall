package com.dfhz.ken.zermall.utils;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dfhz.ken.zermall.R;


/**
 * Created by yili on 2015/10/8.
 * Email : yili270@163.com
 * Description : 操作栏的显示
 *
 */
public class ToolBarUtils {

    public static void show(AppCompatActivity activity, Toolbar toolbar, boolean back, String title) {

        //设置toolbar
        activity.setSupportActionBar(toolbar);

        ActionBar bar = activity.getSupportActionBar();

        //头布局
        View mTvTitle = activity.getLayoutInflater().inflate(R.layout.action_bar_title, null);
        ((TextView) mTvTitle.findViewById(R.id.tv_title)).setText(title);

        //布局参数
        ActionBar.LayoutParams mTitleLp = new ActionBar.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        mTitleLp.gravity = mTitleLp.gravity & ~Gravity.HORIZONTAL_GRAVITY_MASK | Gravity.CENTER_HORIZONTAL;

        bar.setCustomView(mTvTitle, mTitleLp);
        if (back) {
            //是否显示返回操作
            bar.setHomeAsUpIndicator(R.drawable.btn_back);
            optionsUi(bar, bar.DISPLAY_HOME_AS_UP);
        }
        optionsUi(bar, bar.DISPLAY_SHOW_TITLE);
        optionsUi(bar, bar.DISPLAY_SHOW_CUSTOM);

    }

    private static void optionsUi(ActionBar bar, int flags) {
        int change = bar.getDisplayOptions() ^ flags;
        bar.setDisplayOptions(change, flags);
    }
}
