package com.dfhz.ken.zermall.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.view.View;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageSize;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

/**
 * Created by wu on 2015/11/27.
 */
public class SetImage {
    public static ImageLoader ils;

    public static void setimage(Context context, ImageLoader il, String path, ImageView imageView) {
        il.init(DownImageUtils.configImage((Activity) context));
        il.displayImage(path, imageView, DownImageUtils.getOption());
    }

    public static void setimage(Context context, String path, ImageView imageView) {

        ils = ImageLoader.getInstance();
        ils.init(DownImageUtils.configImage((Activity) context));
        ils.displayImage(path, imageView, DownImageUtils.getOption());
    }
    public static void load(Context context, String path, final ImageView imageView) {

        ils = ImageLoader.getInstance();
        ils.init(DownImageUtils.configImage((Activity) context));
        ils.loadImage(path, DownImageUtils.getOption(), new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {

            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                imageView.setImageBitmap(loadedImage);
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {

            }
        });
    }
}
