package com.dfhz.ken.zermall.entity.postbean;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by huangyuekai on 16/11/23.
 */
public class Shopin {
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getOne() {
        return one;
    }

    public void setOne(String one) {
        this.one = one;
    }

    public String getTwo() {
        return two;
    }

    public void setTwo(String two) {
        this.two = two;
    }

    public String getThree() {
        return three;
    }

    public void setThree(String three) {
        this.three = three;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDiscountType() {
        return discountType;
    }

    public void setDiscountType(String discountType) {
        this.discountType = discountType;
    }

    public String getDiscountInfo() {
        return discountInfo;
    }

    public void setDiscountInfo(String discountInfo) {
        this.discountInfo = discountInfo;
    }

    public String getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(String beginTime) {
        this.beginTime = beginTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    /**
     * userId，one，two，three（三张图，不足空字符串代替），describe(描述)，city，address，discountType（优惠方式），
     * discountInfo（优惠信息），beginTime，endTime，typeId（分类），brandId（品牌）
       lat（纬度），lng（经度）
     */
    private String userId;
    private String one;
    private String two;
    private String three;
    private String describe;
    private String city;
    private String address;
    private String discountType;
    private String discountInfo;
    private String beginTime;
    private String endTime;
    private String typeId;
    private String brandId;
    private String lat;
    private String lng;

    public List<NameValuePair> getParaList(){
        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("userId", userId));
        params.add(new BasicNameValuePair("one", one));
        params.add(new BasicNameValuePair("two", two));
        params.add(new BasicNameValuePair("three", three));
        params.add(new BasicNameValuePair("describe", describe));
        params.add(new BasicNameValuePair("city", city));
        params.add(new BasicNameValuePair("address", address));
        params.add(new BasicNameValuePair("discountType", discountType));
        params.add(new BasicNameValuePair("discountInfo", discountInfo));
        params.add(new BasicNameValuePair("beginTime", beginTime));
        params.add(new BasicNameValuePair("endTime", endTime));
        params.add(new BasicNameValuePair("typeId", typeId));
        params.add(new BasicNameValuePair("brandId", brandId));
        params.add(new BasicNameValuePair("lat", lat));
        params.add(new BasicNameValuePair("lng", lng));
        return params;
    }
}
