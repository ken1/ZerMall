package com.dfhz.ken.zermall.utils;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by lqp on 2015/9/2.
 * Email: linqipeng@ycode.cn
 * Description: 调试日志工具类
 */
public class L {
    private L() {
    }

    private static boolean isDebug = true;

    public static void i(String tag, String message) {
        if (isDebug)
            Log.i(tag, "" + message);
    }

    public static void m(Context Context, String message) {
        if (isDebug)
            Toast.makeText(Context, message, Toast.LENGTH_SHORT).show();
    }

    public static void e(String tag, String message) {
        if (isDebug)
            Log.e(tag, message);
    }

    public static void e(Object tag, String message) {
        if (isDebug)
            Log.e(tag.getClass().getName(), message);
    }

    public static void w(String tag, String message) {
        if (isDebug)
            Log.w(tag, message);
    }
}
