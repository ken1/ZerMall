package com.dfhz.ken.zermall.utils.network;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;

import com.dfhz.ken.zermall.constant.InterfaceUrl;
import com.dfhz.ken.zermall.entity.postbean.Shopin;
import com.dfhz.ken.zermall.utils.L;
import com.dfhz.ken.zermall.utils.StringUtil;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.apache.http.Header;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ken.huang on 2016/5/19.
 * 判断网络
*/
public class NetWorkUtil {

    private static ProgressDialog mDialog;
    /**
     * 判断网络是否可用
     *
     * @param context
     * @return
     */
    public static boolean isNetworkConnected(Context context) {
        if (context != null) {
            ConnectivityManager mConnectivityManager = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mNetworkInfo = mConnectivityManager.getActiveNetworkInfo();
            if (mNetworkInfo != null) {
                return mNetworkInfo.isAvailable();
            }
        }
        return false;
    }

    /**
     * 判断WIFI网络是否可用
     *
     * @param context
     * @return
     */
    public static boolean isWifiConnected(Context context) {
        if (context != null) {
            ConnectivityManager mConnectivityManager = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mWiFiNetworkInfo = mConnectivityManager
                    .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            if (mWiFiNetworkInfo != null) {
                return mWiFiNetworkInfo.isAvailable();
            }
        }
        return false;
    }

    /**
     * 判断MOBILE网络是否可用
     *
     * @param context
     * @return
     */
    public static boolean isMobileConnected(Context context) {
        if (context != null) {
            ConnectivityManager mConnectivityManager = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mMobileNetworkInfo = mConnectivityManager
                    .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
            if (mMobileNetworkInfo != null) {
                return mMobileNetworkInfo.isAvailable();
            }
        }
        return false;
    }


    /**
     * 获取当前网络连接的类型信息
     *
     * @param context
     * @return
     */
    public static int getConnectedType(Context context) {
        if (context != null) {
            ConnectivityManager mConnectivityManager = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mNetworkInfo = mConnectivityManager.getActiveNetworkInfo();
            if (mNetworkInfo != null && mNetworkInfo.isAvailable()) {
                return mNetworkInfo.getType();
            }
        }
        return -1;
    }

    public static void showProgressDialog(Context context, String title, String msg) {
        if (mDialog != null && mDialog.isShowing() == true) {
            return;
        }
        mDialog = ProgressDialog.show(context, title, msg, true, false);
    }

    public static void dismissProgressDialog() {
        if (mDialog != null && mDialog.isShowing()) {
            mDialog.dismiss();
        }
    }

    /**
     * 获取商品品牌列表
     *
     * @param callBack
     */
    public static void getBrandList(final SCallBack callBack) {
        final String url = InterfaceUrl.getBrandList;
        AsyncHttpUtil.get(url, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int code, Header[] headers,
                                  byte[] responseBody) {
                L.e("获取品牌列表->", new String(responseBody));
                JSONArray jsonArray = null;
                try {
                    if (responseBody != null && responseBody.length > 0) {
                        JSONObject obj = new JSONObject(new String(
                                responseBody));
                        if (obj == null) {
                            callBack.onFailure("品牌列表获取失败");
                            return;
                        }
                        String errorCode = obj.optString("errorCode");
                        if (!TextUtils.isEmpty(errorCode) && errorCode.equals("1")) {
                            callBack.onSuccess(obj.optString("result"));
                        } else {
                            callBack.onFailure("品牌列表获取失败");
                        }
                    } else {
                        callBack.onFailure("品牌列表获取失败");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    callBack.onFailure("品牌列表获取异常");
                }
            }

            @Override
            public void onFailure(int arg0, Header[] arg1, byte[] arg2,
                                  Throwable arg3) {
                callBack.onFailure("访问受限");
            }
        });

    }

    /**
     * 发布
     *
     * @param newBean
     * @param userId
     * @param callBack
     */
    public static void addShopin(final Shopin newBean, final String userId, final ICallBack callBack) {
        // 发送内容
        new Thread(new Runnable() {
            @Override
            public void run() {// 封装数据
                FabuhttpClient zhuce = new FabuhttpClient();
                try {
                    String str = zhuce.faburubbishifo(newBean.getParaList(),
                            InterfaceUrl.addGoods);
                    L.e("添加服务的返回", str);
                    JSONObject jsonObject = StringUtil
                            .changeJSONObject(str);
                    if (jsonObject != null) {
                        String errorCode = jsonObject
                                .getString("errorCode");
                        if ("1".equals(errorCode)) {
                            callBack.onSuccess();
                        } else {
                            callBack.onFailure();
                        }
                    } else {
                        callBack.onFailure();
                    }
                } catch (ClientProtocolException e) {
                    callBack.onException();
                    e.printStackTrace();
                } catch (IOException e) {
                    callBack.onException();
                    e.printStackTrace();
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    callBack.onException();
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public interface TipListner {
        void onPositive();

        void onNative();
    }

    public interface ICallBack {
        void onSuccess();

        void onFailure();

        void onException();
    }

    public interface SCallBack {
        void onSuccess(String result);

        void onFailure(String result);
    }

    public interface ChatCallBack {
        void onSuccess();

        void onFailure();

        void onIsHei();
    }




}
