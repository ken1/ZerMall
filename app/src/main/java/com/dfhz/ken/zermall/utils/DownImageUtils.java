package com.dfhz.ken.zermall.utils;

import android.app.Activity;
import android.graphics.Bitmap;

import com.dfhz.ken.zermall.R;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;

public class DownImageUtils {

	public static ImageLoaderConfiguration configImage(Activity activity) {

		return new ImageLoaderConfiguration.Builder(activity)

		.memoryCacheExtraOptions(640, 480)

		// 保存每个缓存图片的最大长和宽
				.memoryCache(new WeakMemoryCache())
				.threadPoolSize(3)
				// 线程池的大小 这个其实默认就是3
				.memoryCacheSize(2 * 1024)
				// 设置缓存的最大字节
				.denyCacheImageMultipleSizesInMemory()
				// 缓存显示不同大小的同一张图片
				.imageDownloader(
						new BaseImageDownloader(activity, 5 * 1000, 30 * 1000)) // connectTimeout
				// s)超时时间
				.build();

	}

	public static DisplayImageOptions getOption() {
		return 
				new DisplayImageOptions.Builder()
		.showImageOnLoading(R.drawable.failure_load) // 设置图片在下载期间显示的图片
		.showImageForEmptyUri(R.drawable.failure_nodata)// 设置图片Uri为空或是错误的时候显示的图片
		.showImageOnFail(R.drawable.failure_outtime) // 设置图片加载/解码过程中错误时候显示的图片
		.cacheInMemory(true)// 设置下载的图片是否缓存在内存中
		.cacheOnDisk(true)// 设置下载的图片是否缓存在SD卡中
		.considerExifParams(false) // 是否考虑JPEG图像EXIF参数（旋转，翻转）
		.displayer(new FadeInBitmapDisplayer(0))// 是否图片加载好后渐入的动画时间
//		.displayer(new RoundedBitmapDisplayer(0)) //设置圆角
		.bitmapConfig(Bitmap.Config.RGB_565)//代替ARGB_8888;防止内存溢出RGB_565
		.resetViewBeforeLoading(true)// 设置图片在下载前是否重置，复位
		.build();
	}

}
