package com.dfhz.ken.zermall;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;
import android.view.Display;
import android.view.WindowManager;

import com.baidu.mapapi.SDKInitializer;
import com.dfhz.ken.zermall.utils.L;
import com.dfhz.ken.zermall.utils.SystemUtil;


public class App extends Application {
    public static App instance;
    public SharedPreferences mPreference;

    public void onCreate() {
        super.onCreate();
        instance = this;
//		 JPushInterface.setDebugMode(true);
//        JPushInterface.init(this);
        mPreference = PreferenceManager.getDefaultSharedPreferences(this);

        getSCWidth();
        getSCHeight();

        //初始化百度地图SDK
        SDKInitializer.initialize(getApplicationContext());

    }



    public static App getContext() {
        return instance;
    }
    public void save(String key,String value){
        Editor edit = mPreference.edit();
        edit.putString(key, value);
        edit.commit();
    }

    public void save(String key,boolean value){
        Editor edit = mPreference.edit();
        edit.putBoolean(key, value);
        edit.commit();
    }

    public void save(String key,int value){
        Editor edit = mPreference.edit();
        edit.putInt(key, value);
        edit.commit();
    }

    public boolean inMainProcess() {
        String packageName = getPackageName();
        String processName = SystemUtil.getProcessName(this);
        return packageName.equals(processName);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }



    public int getSCWidth(){
        WindowManager wm = (WindowManager)this.getContext().getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        int w = display.getWidth();
        final float scale =this.getResources().getDisplayMetrics().density;
        L.e("getSCWidth:",w+"");
        return w;
    }

    public int getSCHeight(){
        WindowManager wm = (WindowManager)this.getContext().getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        int w = display.getHeight();
        final float scale =this.getResources().getDisplayMetrics().density;
        L.e("getSCHeight:",w+"");
        return w;
    }

}
