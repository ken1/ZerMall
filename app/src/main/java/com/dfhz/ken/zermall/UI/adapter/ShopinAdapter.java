package com.dfhz.ken.zermall.UI.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.dfhz.ken.zermall.R;
import com.dfhz.ken.zermall.UI.base.BaseMyAdapter1;
import com.dfhz.ken.zermall.utils.MyViewHolder;

import java.util.ArrayList;

/**
 * 类说明:商品适配器呗
 * 
 * @author dumin
 * @date 2015-8-20 下午5:19:30
 */
public class ShopinAdapter extends BaseMyAdapter1<ShopinAdapter.ResultBean> {
	public ShopinAdapter(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}


	@SuppressLint("NewApi")
	@Override
	protected View getExView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder;
		if (convertView == null) {
			convertView = View.inflate(context, R.layout.item_class, null);
			holder = new ViewHolder();
			holder.viewPager = MyViewHolder.get(convertView, R.id.shopin_viewpager);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		//初始化iewPager的内容
		LayoutInflater inflater = LayoutInflater.from(context);
		ImageView view1 = (ImageView) inflater.inflate(R.layout.item_image, null);
		ImageView view2 = (ImageView) inflater.inflate(R.layout.item_image, null);
		ImageView view3 = (ImageView) inflater.inflate(R.layout.item_image, null);
		view1.setImageResource(R.mipmap.logo);
		view2.setImageResource(R.mipmap.ic_launcher);
		view3.setImageResource(R.drawable.bottom_creat);
		ArrayList<ImageView> views = new ArrayList<ImageView>();
		views.add(view1);
		views.add(view2);
		views.add(view3);
		holder.viewPager.setAdapter(new ImageAdapter(views));


		return convertView;
	}
	
	@Override
	protected void onReachBottom() {
		// TODO Auto-generated method stub

	}

	static class ViewHolder {
		ViewPager viewPager;
	}

	public static class ResultBean{
		public ResultBean() {
		}
	}

}
