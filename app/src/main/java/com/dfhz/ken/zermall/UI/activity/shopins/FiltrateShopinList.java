package com.dfhz.ken.zermall.UI.activity.shopins;

import android.view.View;

import com.dfhz.ken.zermall.R;
import com.dfhz.ken.zermall.UI.base.BaseActivity;

/**
 * Created by huangyuekai on 16/11/18.
 * 筛选商品
 */
public class FiltrateShopinList extends BaseActivity {
    @Override
    protected void setMainLayout() {
        setContentView(R.layout.fragment_mine);
    }

    @Override
    protected void initBeforeData() {

    }

    @Override
    protected void initEvents() {

    }

    @Override
    protected void initAfterData() {

    }

    @Override
    public void back(View view) {

    }
}
