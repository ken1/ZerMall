package widget.customview.toolbar;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.GridView;

/**
 * Created by sunwei on 2015/4/24.
 * Email: lx_sunwei@163.com.
 */
public class ExpendedGridView extends GridView {


    public ExpendedGridView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ExpendedGridView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public ExpendedGridView(Context context) {
        super(context);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int expandSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2,
                MeasureSpec.AT_MOST);
        super.onMeasure(widthMeasureSpec, expandSpec);
    }
}
