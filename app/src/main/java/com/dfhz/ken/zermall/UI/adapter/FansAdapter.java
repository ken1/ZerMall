package com.dfhz.ken.zermall.UI.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.dfhz.ken.zermall.R;
import com.dfhz.ken.zermall.UI.base.BaseMyAdapter1;

/**
 * 类说明:我的关注着、关注我者
 * 
 * @author dumin
 * @date 2015-8-20 下午5:19:30
 */
public class FansAdapter extends BaseMyAdapter1<FansAdapter.ResultBean> {
	public FansAdapter(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}


	@SuppressLint("NewApi")
	@Override
	protected View getExView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder;
		if (convertView == null) {
			convertView = View.inflate(context, R.layout.item_class, null);
			holder = new ViewHolder();

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}


		return convertView;
	}
	
	@Override
	protected void onReachBottom() {
		// TODO Auto-generated method stub

	}

	static class ViewHolder {

	}

	public static class ResultBean{
		public ResultBean() {
		}
	}

}
