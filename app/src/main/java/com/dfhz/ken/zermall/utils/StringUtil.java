package com.dfhz.ken.zermall.utils;

import android.content.Context;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Base64;


import com.dfhz.ken.zermall.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Locale;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtil {

	/*
	 * 解析字符串获取验证码
	 */
	public static String getCode(String json) {
		if ("".equals(json) || null == json) {
			return "";
		} else {
			return json.split(",")[0].split(":")[1];
		}
	}

	/*
	 * 说明：移动：134、135、136、137、138、139、150、151、157(TD)、158、159、187、188
	 * 联通：130、131、132、152、155、156、185、186 电信：133、153、180、189
	 * 总结起来就是第一位必定为1，第二位必定为3或5或8，其他位置的可以为0-9 验证号码 手机号 固话均可 作者：丁国华 2015年9月20日
	 * 13:52:35
	 */
	public static boolean isPhoneNumberValid(String phoneNumber) {
		boolean isValid = false;
		// String expression =
		// "((^(13|15|18)[0-9]{9}$)|(^0[1,2]{1}\\d{1}-?\\d{8}$)|(^0[3-9] {1}\\d{2}-?\\d{7,8}$)|(^0[1,2]{1}\\d{1}-?\\d{8}-(\\d{1,4})$)|(^0[3-9]{1}\\d{2}-? \\d{7,8}-(\\d{1,4})$))";
		String expression = "(^1[0-9]{10}$)";
		CharSequence inputStr = phoneNumber;
		Pattern pattern = Pattern.compile(expression);
		Matcher matcher = pattern.matcher(inputStr);
		if (matcher.matches()) {
			isValid = true;
		}
		return isValid;
	}

	// 正则表达式判断6位数字
	public static boolean isNumberValid(String phoneNumber) {
		boolean isValid = false;
		String expression = "^\\d{6}$";
		CharSequence inputStr = phoneNumber;
		Pattern pattern = Pattern.compile(expression);
		Matcher matcher = pattern.matcher(inputStr);
		if (matcher.matches()) {
			isValid = true;
		}
		return isValid;
	}

	public static JSONObject changeJSONObject(String jsonStr) {

		JSONObject jsonObject = null;
		try {
			jsonObject = new JSONObject(jsonStr);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonObject;

	}

	public static String getPercentString(float percent) {
		return String.format(Locale.US, "%d%%", (int) (percent * 100));
	}

	/**
	 * 删除字符串中的空白符
	 * 
	 * @param content
	 * @return String
	 */
	public static String removeBlanks(String content) {
		if (content == null) {
			return null;
		}
		StringBuilder buff = new StringBuilder();
		buff.append(content);
		for (int i = buff.length() - 1; i >= 0; i--) {
			if (' ' == buff.charAt(i) || ('\n' == buff.charAt(i))
					|| ('\t' == buff.charAt(i)) || ('\r' == buff.charAt(i))) {
				buff.deleteCharAt(i);
			}
		}
		return buff.toString();
	}

	/**
	 * 获取32位uuid
	 * 
	 * @return
	 */
	public static String get32UUID() {
		return UUID.randomUUID().toString().replaceAll("-", "");
	}

	public static boolean isEmpty(String input) {
		return TextUtils.isEmpty(input);
	}

	/**
	 * 生成唯一号
	 * 
	 * @return
	 */
	public static String get36UUID() {
		UUID uuid = UUID.randomUUID();
		String uniqueId = uuid.toString();
		return uniqueId;
	}


	public static final String filterUCS4(String str) {
		if (TextUtils.isEmpty(str)) {
			return str;
		}

		if (str.codePointCount(0, str.length()) == str.length()) {
			return str;
		}

		StringBuilder sb = new StringBuilder();

		int index = 0;
		while (index < str.length()) {
			int codePoint = str.codePointAt(index);
			index += Character.charCount(codePoint);
			if (Character.isSupplementaryCodePoint(codePoint)) {
				continue;
			}

			sb.appendCodePoint(codePoint);
		}

		return sb.toString();
	}

	/**
	 * counter ASCII character as one, otherwise two
	 * 
	 * @param str
	 * @return count
	 */
	public static int counterChars(String str) {
		// return
		if (TextUtils.isEmpty(str)) {
			return 0;
		}
		int count = 0;
		for (int i = 0; i < str.length(); i++) {
			int tmp = (int) str.charAt(i);
			if (tmp > 0 && tmp < 127) {
				count += 1;
			} else {
				count += 2;
			}
		}
		return count;
	}
	
	public static int getColor(int position){
		int result = R.color.msg_color1;
		switch (position) {
		case 0:
			 result = R.color.msg_color1;
			break;
		case 1:
			result = R.color.msg_color2;
			break;
		case 2:
			result = R.color.msg_color3;
			break;
		case 3:
			result = R.color.msg_color4;
			break;
		case 4:
			result = R.color.msg_color5;
			break;
		case 5:
			result = R.color.msg_color6;
			break;
		default:
			result = R.color.msg_color6;
			break;
		}
		return result;
	}

	/**
	 * 检测是否有emoji表情
	 * @param source
	 * @return
	 */
	public static boolean containsEmoji(String source) {
		int len = source.length();
		for (int i = 0; i < len; i++) {
			char codePoint = source.charAt(i);
			if (!isEmojiCharacter(codePoint)) { // 如果不能匹配,则该字符是Emoji表情
				return true;
			}
		}
		return false;
	}


	/**
	 * 判断是否是Emoji
	 *
	 * @param codePoint
	 *            比较的单个字符
	 * @return
	 */
	private static boolean isEmojiCharacter(char codePoint) {
		return (codePoint == 0x0) || (codePoint == 0x9) || (codePoint == 0xA)
				|| (codePoint == 0xD)
				|| ((codePoint >= 0x20) && (codePoint <= 0xD7FF))
				|| ((codePoint >= 0xE000) && (codePoint <= 0xFFFD))
				|| ((codePoint >= 0x10000) && (codePoint <= 0x10FFFF));
	}

	/**
	 * base64编码
	 * @param str
	 * @return
     */
	public static String getBase64(String str) {
		String content_8 = "";
		try {
			content_8 = URLEncoder.encode(str, "utf-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String strBase64 = new String(Base64.encode(content_8.getBytes(), Base64.DEFAULT));
		return strBase64;
	}

	public static String getDeviceToken(Context context){
		String deviceToken = "001";
		try {
			TelephonyManager TelephonyMgr = (TelephonyManager)context.getSystemService(context.TELEPHONY_SERVICE);
			 deviceToken = TelephonyMgr.getDeviceId();
		}catch (Exception e){
			e.printStackTrace();
		}

		return deviceToken;
	}
}
