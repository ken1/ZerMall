package widget.customview;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * @author:jeney
 * @date:2015/4/22
 * @todo:禁止手势滑动的viewpager
 */
public class ProhibitScrollViewPager extends ViewPager {


    public ProhibitScrollViewPager(Context context) {
        super(context);
    }

    public ProhibitScrollViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return false;
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        return false;
    }
}
