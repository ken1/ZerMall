package com.dfhz.ken.zermall.UI.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.search.core.SearchResult;
import com.baidu.mapapi.search.poi.OnGetPoiSearchResultListener;
import com.baidu.mapapi.search.poi.PoiCitySearchOption;
import com.baidu.mapapi.search.poi.PoiDetailResult;
import com.baidu.mapapi.search.poi.PoiIndoorResult;
import com.baidu.mapapi.search.poi.PoiResult;
import com.baidu.mapapi.search.poi.PoiSearch;
import com.dfhz.ken.zermall.R;
import com.dfhz.ken.zermall.utils.L;

/**
 * Created by huangyuekai on 16/11/28.
 */
public class MapActivity extends Activity {
    EditText searchWords = null;
    Button commit = null;
    MapView mMapView = null;
    PoiSearch mPoiSearch = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //在使用SDK各组件之前初始化context信息，传入ApplicationContext
        setContentView(R.layout.activity_map);
        //获取地图控件引用
        mMapView = (MapView) findViewById(R.id.bmapView);
        searchWords = (EditText)findViewById(R.id.edt_search);
        commit = (Button)findViewById(R.id.btn_search);
        mPoiSearch = PoiSearch.newInstance();
        mPoiSearch.setOnGetPoiSearchResultListener(poiListener);
        commit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPoiSearch.searchInCity((new PoiCitySearchOption())
                        .city("苏州市")
                        .keyword(searchWords.getText().toString())
                        );
            }
        });
    }

    OnGetPoiSearchResultListener poiListener = new OnGetPoiSearchResultListener(){
        public void onGetPoiResult(PoiResult result){
            //获取POI检索结果
            if (result.error == SearchResult.ERRORNO.NO_ERROR){

            }
                if(result != null) {
                if(result.getAllPoi() != null) {
                    if (result.getAllPoi().size() > 0) {
                        L.e("搜索的结果",result.getAllPoi().get(0).address);
                        Toast.makeText(MapActivity.this, result.getAllPoi().get(0).name, Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(MapActivity.this, "没有2", Toast.LENGTH_LONG).show();
                    }
                }else{
                    Toast.makeText(MapActivity.this, "没有1", Toast.LENGTH_LONG).show();
                }
            }else{
                Toast.makeText(MapActivity.this, "空的0", Toast.LENGTH_LONG).show();
            }

        }
        public void onGetPoiDetailResult(PoiDetailResult result){
            //获取Place详情页检索结果

        }

        @Override
        public void onGetPoiIndoorResult(PoiIndoorResult poiIndoorResult) {

        }
    };
    @Override
    protected void onDestroy() {
        super.onDestroy();
        //在activity执行onDestroy时执行mMapView.onDestroy()，实现地图生命周期管理
        mMapView.onDestroy();
        mPoiSearch.destroy();
    }
    @Override
    protected void onResume() {
        super.onResume();
        //在activity执行onResume时执行mMapView. onResume ()，实现地图生命周期管理
        mMapView.onResume();
    }
    @Override
    protected void onPause() {
        super.onPause();
        //在activity执行onPause时执行mMapView. onPause ()，实现地图生命周期管理
        mMapView.onPause();
    }
}
