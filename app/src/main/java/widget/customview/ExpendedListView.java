package widget.customview;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ListView;

/**
 * 展开的listview
 */

public class ExpendedListView extends ListView {

	public ExpendedListView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public ExpendedListView(Context context) {
		super(context);
	}

	public ExpendedListView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

		int expandSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2,
				MeasureSpec.AT_MOST);
		super.onMeasure(widthMeasureSpec, expandSpec);
	}
}
