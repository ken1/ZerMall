package com.dfhz.ken.zermall.utils;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by yili on 2015/10/9.
 * Email : yili270@163.com
 * Description : 提示的工具类
 */
public class ToastUtil {
    public static boolean isShow = true;

    public static void show(Context context, String content) {
        if (null != context && isShow) {
            Toast toast = Toast.makeText(context, content, Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    public static void show(Context context, int content) {
        if (null != context && isShow) {
            Toast toast = Toast.makeText(context, content, Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    public static void logE(String tag, String msg) {
        if (msg == null && isShow) {
            msg = "";
        }
        Log.d(tag, msg);
    }
}