package com.dfhz.ken.zermall.UI.base;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dfhz.ken.zermall.utils.L;

import butterknife.ButterKnife;

/**
 * Created by huangyuekai on 16/11/17.
 */
public abstract class BaseFragment extends Fragment implements View.OnClickListener {
    public View rootView;
    public int layoutId;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setLayoutId();
        rootView = inflater.inflate(layoutId, null);
        ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        initBeforeData();
        initEvents();
        initAfterData();
    }

    protected abstract void setLayoutId();

    /**
     * 初始化先前数据
     */
    protected abstract void initBeforeData();

    /**
     * 初始化事件
     */
    protected abstract void initEvents();

    /**
     * 初始化之后数据
     */
    protected abstract void initAfterData();

    /**
     * 含有Bundle通过Action跳转界面 *
     */
    protected void startActivity(String action, Bundle bundle) {
        Intent intent = new Intent();
        intent.setAction(action);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
    }

    /**
     * 通过Class跳转界面 *
     */
    protected void startActivity(Class<?> cls) {
        startActivity(cls, null);
    }

    /**
     * 含有Bundle通过Class跳转界面 *
     */
    protected void startActivity(Class<?> cls, Bundle bundle) {
        Intent intent = new Intent();
        intent.setClass(getActivity(), cls);
        if (bundle != null) {
            intent.putExtra("bundle", bundle);
        }
        startActivity(intent);
    }

    @CallSuper
    protected void onStopss() {
        // 如果不调用此方法，则按home键的时候会出现图标无法显示的情况。
        L.e(getActivity(),"@CallSuper");
    }


    @Override
    public void onClick(View v) {

    }
}
