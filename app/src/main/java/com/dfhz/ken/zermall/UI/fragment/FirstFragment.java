package com.dfhz.ken.zermall.UI.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dfhz.ken.zermall.R;
import com.dfhz.ken.zermall.UI.base.BaseFragment;
import com.dfhz.ken.zermall.UI.base.BaseMyAdapter;

import butterknife.Bind;
import butterknife.ButterKnife;
import widget.AutoScrollViewPager;
import widget.swipemenulistview.SwipeMenuListView;

/**
 * Created by huangyuekai on 16/11/17.
 */
public class FirstFragment extends BaseFragment {

    private View headView;
    private SwipeMenuListView mListView;

    @Override
    protected void setLayoutId() {
        layoutId = R.layout.fragment_first;
    }

    @Override
    protected void initBeforeData() {
        headView = View.inflate(getActivity(), R.layout.header_first, null);
        mListView = (SwipeMenuListView) rootView.findViewById(R.id.data_list);
        mListView.addHeaderView(headView);
        mListView.setAdapter(new nullAdapter(getActivity()));
    }

    @Override
    protected void initEvents() {

    }

    @Override
    protected void initAfterData() {

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    class nullAdapter extends BaseMyAdapter {

        public nullAdapter(Context context) {
            super(context);
        }

        @Override
        protected View getExView(int position, View convertView, ViewGroup parent) {
            return null;
        }

        @Override
        protected void onReachBottom() {

        }
    }
}
