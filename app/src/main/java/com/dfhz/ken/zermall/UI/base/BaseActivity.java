package com.dfhz.ken.zermall.UI.base;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.Window;
import android.widget.Toast;


import java.util.Calendar;

import butterknife.ButterKnife;


/**
 * 类说明 基类
 * @author wangsheng
 * @date 2015-8-20 下午2:56:04
 */
@SuppressLint("NewApi")
public abstract class BaseActivity extends FragmentActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setMainLayout();
		initBeforeData();
		initEvents();
		initAfterData();

	}

	/**
	 * 初始化布局
	 */
	protected abstract void setMainLayout();

	/**
	 * 初始化先前数据
	 */
	protected abstract void initBeforeData();

	/**
	 * 初始化事件
	 */
	protected abstract void initEvents();

	/**
	 * 初始化之后数据
	 */
	protected abstract void initAfterData();

	/**
	 * 含有Bundle通过Action跳转界面 *
	 */
	protected void startActivity(String action, Bundle bundle) {
		Intent intent = new Intent();
		intent.setAction(action);
		if (bundle != null) {
			intent.putExtras(bundle);
		}
		startActivity(intent);
	}

	/**
	 * 通过Class跳转界面 *
	 */
	protected void startActivity(Class<?> cls) {
		startActivity(cls, null);
	}

	/**
	 * 含有Bundle通过Class跳转界面 *
	 */
	protected void startActivity(Class<?> cls, Bundle bundle) {
		Intent intent = new Intent();
		intent.setClass(this, cls);
		if (bundle != null) {
			intent.putExtra("bundle", bundle);
		}
		startActivity(intent);
	}

	/**
	 * 短暂显示Toast提示(来自String) *
	 */
	public void showShortToast(String text) {
		Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
	}

	/**
	 * 短暂显示Toast提示(来自res) *
	 */
	protected void showShortToast(int resId) {
		Toast.makeText(this, resId, Toast.LENGTH_SHORT).show();
	}

	/**
	 * 初始化布局
	 */
	public abstract void back(View view);




	@Override
	protected void onStop() {
		// 如果不调用此方法，则按home键的时候会出现图标无法显示的情况。
		super.onStop();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	public abstract class NoDoubleClickListener implements View.OnClickListener {

		public static final int MIN_CLICK_DELAY_TIME = 1000;
		private long lastClickTime = 0;

		@Override
		public void onClick(View v) {
			long currentTime = Calendar.getInstance().getTimeInMillis();
			if (currentTime - lastClickTime > MIN_CLICK_DELAY_TIME) {
				lastClickTime = currentTime;
				onNoDoubleClick(v);
			}
		}
	}

	private void onNoDoubleClick(View view){

	}
}


